---
date: "2025-02-05T12:00:00"
title: Recursos contra las crisis ambientales
---

# Documentales recomendados

- Sobre la crisis climática y el antropocéno. *Where on Earth are we going?* [Link](https://www.youtube.com/watch?v=HvD0TgE34HA) . Recomiendo activar los subtitulos autogenerados en español
- Sobre la problemática del plástico en los oceanos. *Un océano de plástico* [Link](https://www.youtube.com/watch?v=WYTHgxHGPYg)

# Para difundir la informacion sobre las crisis

- Herramienta para visualizar el cambio climático en tu ciudad y difundirlo mediante gráficas sencillas. *Show your stripes* [Link](https://showyourstripes.info/)
  

# Sobre ciencia

- Sitio web de la red de científicos que monitorean la captura y emisión de gases de efecto invernadero en México. Red MexFlux [Link](https://mexflux.gitlab.io/) . Recomiendo leer su boletín mensual.
