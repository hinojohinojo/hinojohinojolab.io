---
date: "2023-10-01T22:25:00"
title: Literatura base por temas
---

# Las características de las plantas están coordinadas

- **El espectro económico de la hoja (2004)**: Muchas características que controlan la fisiología de la hoja están relacionadas entre si, es decir, están coordinadas. Así hojas de fisiología lenta tienen ciertos valores de características "lentas" y las hojas de fisiología rápida tienen valores de características "rápidas" https://doi.org/10.1038/nature02403
- **El espectro lento-rápido de las plantas (2014)**: Resulta que no solo las características de las hoja están coordinadas, sino que pareciera que las características de toda la planta lo están. Así plantas de fisióloga/crecimiento rapado tienen características de raíz, tallo, y hojas "rápidos", y lo opuesto con las de fisiología/crecimiento lento. https://doi.org/10.1111/1365-2745.12211
- El espectro de la forma y función de las plantas: https://doi.org/10.1038/nature16489 https://doi.org/10.1038/s41597-022-01774-9

*Ambos no son un dogma, y han sido bastante criticados, pero son un interesante principio ecológico que nos permite simplificar la biodiversidad de plantas y que cosas regulan sus procesos fisiologicos y ecológicos*


# ¿Qué características controlan la fisiología de las hojas?
- Las del espectro económico de la hoja: https://doi.org/10.1038/nature02403
- La clorofila https://doi.org/10.1111/gcb.13599
- ¿El contenido de agua? https://doi.org/10.1038/s41467-022-32784-1


# ¿Qué características controlan la fisiología de la vegetación?
- LAI y nitrógeno https://doi.org/10.1098/rspb.2011.2270
- N https://doi.org/10.1016/j.jag.2015.05.009 https://doi.org/10.1002/ece3.2479 https://doi.org/10.1111/gcb.15385
- Clorofila https://doi.org/10.1002/2015JG002980 https://doi.org/10.1111/gcb.14624 
- clorofila/carotenoides https://doi.org/10.1073/pnas.1606162113


# Introducción al uso de imágenes de satélite
- https://doi.org/10.1016/j.rse.2019.111383
- https://doi.org/10.1038/s43017-022-00298-5
- https://doi.org/10.1111/nph.15934

# ¿Cómo las características de las plantas modifican la reflectancia de la superficie?
- Según lo que sabemos (bueno como introducción) https://doi.org/10.1007/978-3-030-33157-3_14
- Según conocimiento y modelos https://doi.org/10.1111/j.1469-8137.2010.03536.x
- Según modelos https://doi.org/10.1016/j.rse.2008.01.026 (Figs 3, 4, y 5)


# ¿Qué miden realmente los índices de vegetación?
- NDVI, NIRv y EVI https://doi.org/10.3390/rs12091405
- Indices de clorofila, agua y LAI https://doi.org/10.3390/rs11202418
