---
title: R basics tutorial
author: 'Cesar Hinojo Hinojo'
date: '2020-06-28'
slug: r-basics-1
categories: [R, tutorial, course]
tags: [R, tutorial, course]
excerpt: "This is a brief introduction to the very basics of R and some common tasks for data manipulation"
---

<script src="/rmarkdown-libs/header-attrs/header-attrs.js"></script>


<p><em>This is a brief introduction to the very basics of R and some common tasks for data manipulation</em></p>
<div id="what-is-r-and-how-does-it-work" class="section level1">
<h1>What is R and how does it work?</h1>
<p>R is two things: a programming language and a software to interpret that language. The goal of both is to give us tons of ways to process and analyse data.</p>
<p>The basic of how R work is this. In general, we may have 1) our data or some information (or a formula, a model, a single value, or many other things) stored in an <em>object</em> in the computer’s memory, 2) and we will do an operation with it, most likely in the form of a <em>function</em>, 3) the <em>arguments</em> of the function specify the particular details about the operations we are going to perform, 4) and this will give us some <em>results</em>, which we can print on the screen or store in another <em>object</em> to do other things with the results, or export it.</p>
<p>Say you have a table with stem diameters and total biomass of trees of your forest study site, which is stored in an <em>object</em> called <code>trees</code>, and you want to plot this data. The <em>function</em> could be <code>plot</code> and the <em>arguments</em> of that function will specify on which axis each variable will be plotted, the type of the plot (e.g. scatterplot) and the color of the symbols that represent the data, and the <em>results</em> of that (i.e. the plot) will be shown on your screen.</p>
<div id="illustrating-this-example" class="section level2">
<h2>Illustrating this example</h2>
<p>We will input some data on tree stem diameters and biomass and then plot it. We will explain the details later.</p>
<pre class="r"><code># Here we define two objects with the data
diameter &lt;- c(2, 7, 9, 15, 20, 33)
biomass &lt;- c(1, 23, 30, 60, 85, 153)

# Plot defining the roles of the data, type of plot (points), and shape and color of the symbols
plot(x = diameter, y = biomass, type = &quot;p&quot;, pch = 20, col = &quot;black&quot;)</code></pre>
<div class="figure"><span id="fig:testPlot"></span>
<img src="/post/2020-06-28-r-basics_files/figure-html/testPlot-1.png" alt="A plot of stem diameter vs biomass in forest trees" width="384" />
<p class="caption">
Figure 1: A plot of stem diameter vs biomass in forest trees
</p>
</div>
<p>That was very easy!</p>
</div>
</div>
<div id="why-use-r" class="section level1">
<h1>Why use R?</h1>
<ul>
<li>Its free in every possible way</li>
<li>Available in any operating system and its usable even in old computers</li>
<li>Infinite possibilities for your analysis via R packages. R packages extends R functionality, and can be created by the same people that develop the analysis techniques</li>
<li>You can do all steps of your analyses in the same software</li>
<li>Can do high quality plots and technical/scientific reports</li>
<li>Can help doing science reproducible, by sharing your code and allowing others to replicate.</li>
<li>Having some much possibilities at you reach makes you want to learn more and more</li>
</ul>
</div>
<div id="on-rstudio" class="section level1">
<h1>On Rstudio</h1>
<p>Rstudio is a software that can make it kind of easier to use R. It gives you several tools in one same place such as a code editor (for writing the code for your analysis), the R console (where the code is run), and places to look at the help files, plots, the objects in the memory, available packages, etc.</p>
<div class="figure">
<img src="../images/rstudio_components.png" style="width:100.0%" alt="" />
<p class="caption">Figure 2: Components of Rstudio</p>
</div>
<p>Interestingly, Rstudio is not the only software for easily working with R. We will see some more in the next part of the course.</p>
</div>
<div id="basic-objectsinformation-types-and-its-creation" class="section level1">
<h1>Basic objects/information types and its creation</h1>
<p>Data in R can be in several types. The most common ones are <em>logic</em> (either <code>TRUE</code> or <code>FALSE</code>), <em>character</em>, and <em>numeric</em> (can be others, like dates). Then, the data can be in different types of objects. The most common ones are 1) <em>single values</em>, no need to explain, 2) <em>vectors</em>, a collection of values of the same type, 3) <em>data frames</em>, are tables composed of two or more vectors, 4) <em>matrices</em>, and 4) <em>lists</em>, which can contain any of the other types.</p>
<p>For creating any object we choose a name for it followed by <code>&lt;-</code> and then put what the object is. Functions would look like this <code>function(argument1 = value, argument2 = value, argument3 = value)</code>. We can print the objects on the console by running the object name. Object names should typically not be quoted.</p>
<p>We create single values like this</p>
<pre class="r"><code># logicals (TRUE or FALSE, in capital letters)
logic1 &lt;- TRUE

# numeric
numeric1 &lt;- 1

# character (always single or double quoted)
char &lt;- &quot;sample text&quot;</code></pre>
<p>There are several ways to create vectors</p>
<pre class="r"><code># numeric vectors
vector1 &lt;- c(1, 2, 3, 4)
vector2 &lt;- seq(from = 1, to = 4, by = 1)
vector3 &lt;- 1:4

# character and logical
vector4 &lt;- c(&quot;Pinus&quot;, &quot;Juniperus&quot;, &quot;Quercus&quot;, &quot;Salvia&quot;)
vector5 &lt;- c(TRUE, FALSE, FALSE, TRUE)</code></pre>
<p>To create a data frame</p>
<pre class="r"><code># from existing vectors
dataframe1 &lt;- data.frame(vector1, vector4, vector5)
dataframe1</code></pre>
<pre><code>##   vector1   vector4 vector5
## 1       1     Pinus    TRUE
## 2       2 Juniperus   FALSE
## 3       3   Quercus   FALSE
## 4       4    Salvia    TRUE</code></pre>
<pre class="r"><code># from existing vectors, changing column names
dataframe2 &lt;- data.frame(number = vector1, genera = vector4, alive = vector5)
dataframe2</code></pre>
<pre><code>##   number    genera alive
## 1      1     Pinus  TRUE
## 2      2 Juniperus FALSE
## 3      3   Quercus FALSE
## 4      4    Salvia  TRUE</code></pre>
<pre class="r"><code># created at the moment
dataframe3 &lt;- data.frame(number = c(1, 2), genera = c(&quot;Pinus&quot;, &quot;Juniperus&quot;), alive = c(TRUE, FALSE))
dataframe3</code></pre>
<pre><code>##   number    genera alive
## 1      1     Pinus  TRUE
## 2      2 Juniperus FALSE</code></pre>
<p>To create a matrix</p>
<pre class="r"><code>matrix1 &lt;- matrix(data = c(2, 5, 3, 7, 10, 8), nrow = 2, ncol = 3, byrow = FALSE) 
matrix1</code></pre>
<pre><code>##      [,1] [,2] [,3]
## [1,]    2    3   10
## [2,]    5    7    8</code></pre>
<p>To create a list</p>
<pre class="r"><code># A named list
list1 &lt;- list(sValue = logic1, sValue2 = numeric1, plants = dataframe2, matrix = matrix1)
list1</code></pre>
<pre><code>## $sValue
## [1] TRUE
## 
## $sValue2
## [1] 1
## 
## $plants
##   number    genera alive
## 1      1     Pinus  TRUE
## 2      2 Juniperus FALSE
## 3      3   Quercus FALSE
## 4      4    Salvia  TRUE
## 
## $matrix
##      [,1] [,2] [,3]
## [1,]    2    3   10
## [2,]    5    7    8</code></pre>
<p><strong>Excercise 1</strong></p>
<p>If you are interested in practicing, try this: create a dataframe of 10 rows, at least one column of each data type (logic, numeric, and character), the character column should have some repeated elements, include a few missing values here and there (<code>NA</code>).</p>
</div>
<div id="good-practices-for-coding" class="section level1">
<h1>Good practices for coding</h1>
<p>Before jumping into more coding action, here is my advice for some minimal good practice for coding.</p>
<ul>
<li>Comment your code ( add comments with <code>#</code>) whenever is needed. This will help you in the future when you come back and try to understand the code. When you are just starting to code in R, its good to add comments on everything, but as practice goes well, try only commenting to clarify certain parts of your code. Code purists may argue that the code should be readable by itself (if its well written and structured).</li>
<li>Use descriptive names for your objects. Avoid generic names. As you code you will find descriptive names easier to use and remember what exactly are they.</li>
<li>Explicitly write the argument name of the functions. This is not strictly needed but makes code easier to read and understand.</li>
<li>Leave spaces. E.g. <code>2 + 2</code> instead of <code>2+2</code> <code>objName &lt;- c(1, 3)</code> instead of <code>objName&lt;-c(1,3)</code>. Spaced code looks cleaner and its easier to read.</li>
<li>Follow help pages. To use the help, type function names in the Rstudio help tab (see Figure 2 or run <code>?functionName</code> in the console. More explanation on this below.</li>
</ul>
</div>
<div id="the-help-files" class="section level1">
<h1>The help files</h1>
<p>The help files have a very specific structure. Its a good idea to get a good understanding on what each part of the help is. Try searching for the help for the <code>mean()</code> function (e.g. type ?mean in the console or just search mean in Rstudio help).</p>
<p>At the top most part you will find the name of the function, what package does it belogs to, and a human readable name and explanation of the function. After that there are certain sections:</p>
<ul>
<li><strong>Usage</strong>: gives you a general sense of how to use the function. In addition, pay attention to the order of the arguments. This is the order on which you should input them if you decide not to spell out explicitly the argument names in your code. Also, pay attention to which arguments have <code>=</code> and a value for that argument… those values are their default values, i.e. if you do not write them, that is the value those arguments will take.</li>
<li><strong>Agruments</strong>: Gives an explanation of what each argument is and the specific type of information and object type that you should input. E.g. in mean() function, the argument <code>x</code> expects a vector as input, while <code>na.rm</code> expects a logical value (<code>TRUE</code> or <code>FALSE</code>)</li>
<li><strong>Details</strong>: Gives further details about what the function does, and specifics on certain arguments.</li>
<li><strong>Value</strong>: Explain what kind of data/object/information you should expect as the output.</li>
</ul>
<p>The help also gives some references, related functions and examples.</p>
</div>
<div id="some-very-common-functions" class="section level1">
<h1>Some very common functions</h1>
<p>The following functions are used quite often, so its good for you to know them. You should take some time to read their help files.</p>
<ul>
<li><strong><code>sum()</code></strong>, <strong><code>mean()</code></strong>, <strong><code>max()</code></strong>, <strong><code>min()</code></strong>: These functions are almost self-explanatory. Be sure to check the help file to know how to use them.</li>
<li><strong><code>quantile()</code></strong>: Gives the quantiles of a vector, which are quite useful to get a quick picture of the distribution of our data.</li>
<li><strong><code>summary()</code></strong>: Its a generic function that gives a summary on a lot of objects, such as vectors and dataframes, providing key statistical and data summaries. Also if applied to other objects (e.g. statistical models), it gives you a summary of the results.</li>
</ul>
<p>Lets try them one of these in our dataframe of plants that we created before.</p>
<pre class="r"><code># A dataframe of tree stem diameter and biomass
diameter &lt;- c(2, 7, 9, 15, 20, 33)
biomass &lt;- c(1, 23, 30, NA, 85, 153)
treesDF &lt;- data.frame(diameter, biomass)

# Get a summary of this dataframe
summary(treesDF)</code></pre>
<pre><code>##     diameter        biomass     
##  Min.   : 2.00   Min.   :  1.0  
##  1st Qu.: 7.50   1st Qu.: 23.0  
##  Median :12.00   Median : 30.0  
##  Mean   :14.33   Mean   : 58.4  
##  3rd Qu.:18.75   3rd Qu.: 85.0  
##  Max.   :33.00   Max.   :153.0  
##                  NA&#39;s   :1</code></pre>
<p>Lets try the mean for <code>biomass</code>. We can select a specific column with <code>$</code> like this: <code>objectName$columnName</code>.</p>
<pre class="r"><code># If we don&#39;t include `na.rm = TRUE` we would get a weird
# result because `biomass` data has a missing value
mean(x = treesDF$biomass)</code></pre>
<pre><code>## [1] NA</code></pre>
<pre class="r"><code>mean(x = treesDF$biomass, na.rm = TRUE)</code></pre>
<pre><code>## [1] 58.4</code></pre>
<p><strong>Excercise 2</strong>: Try using one of this functions over one column of the dataframe you created in <strong>Excercise 1</strong>.</p>
</div>
<div id="selecting-data" class="section level1">
<h1>Selecting data</h1>
<p>We can have more specific selections of our data, and there are several ways to do it. One, is specifying the specific locations of our data, and another one is selecting data that meets certain conditions.</p>
<pre class="r"><code># Different ways of selecting by specifying data locations
treesDF$diameter[3] # [rowNumber] 
treesDF[3, 2] # [rowNumber, columnNumber]
treesDF[1:3, 2] # several rows from a column
treesDF[1:3, ] # several rows from all columns</code></pre>
<p><strong>Excercise 3</strong>: With your dataframe from <strong>Excercise 1</strong>, try to find out how can you select rows 3, 5, 8 from all columns, and how can you select all rows from columns 2 and 3.</p>
<p>For selecting data that meets certain conditions, we will define conditions based on the following logical operators:</p>
<ul>
<li><code>&gt;</code> or <code>&lt;</code> for higher or lower than</li>
<li><code>&gt;=</code> or <code>&lt;=</code> for equal or higher/lower than</li>
<li><code>==</code> or <code>!=</code> for equal or not equal</li>
<li><code>&amp;</code> or <code>|</code> for AND or OR</li>
</ul>
<p>You will build conditions by comparing objects/information from the right side of the operator to the ones in the left side of the operator.</p>
<p>A useful function for selecting data based on coditions is <code>subset()</code>. Take a look at its help file.</p>
<pre class="r"><code># On our tree dataframe, lets select the bigger trees.
# That is, e.g. trees with biomass larger than 50
# The argument subset states the condition
biggerTrees &lt;- subset(x = treesDF, subset = treesDF$biomass &gt; 50)
biggerTrees</code></pre>
<pre><code>##   diameter biomass
## 5       20      85
## 6       33     153</code></pre>
<p><strong>Excercise 4</strong>: From the dataframe you created in <strong>Excercise 1</strong>, subset it using a condition. Try also calculating the mean of a column of the subseted dataframe.</p>
</div>
<div id="what-did-we-learn-in-this-session" class="section level1">
<h1>What did we learn in this session?</h1>
<ul>
<li>R can be an amazing tool for data analysis</li>
<li>R stores information in objects, and process with functions and arguments.</li>
<li>Data can be stored as single values, vectors (collection of values), dataframes (a typical data table, like the ones you do in excel), matrices and lists (collection of any of the other types).</li>
<li>Good practices for coding are commenting, using spaces in your code, use descriptive names, spell out argument names.</li>
<li>Help files have a lot of useful information about how to use the functions.</li>
<li>Some basic functions: mean(), sum(), summary(),subset()</li>
<li>We can select data with $, [], and subset()</li>
</ul>
<p>Next session will be about plotting with ggplot, and statistical analyses, so real data analysis action.</p>
</div>
